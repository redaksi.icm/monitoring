<?php

/**
 * Created by PhpStorm.
 * Project:   monitoring.
 * File Name: MY_Form_validation.php.
 * User:      AcenetDev
 * Date:      8/4/2017
 * Time:      3:48 PM
 */
class MY_Form_validation extends CI_Form_validation {

	public function __construct(array $rules = array() ) {
		parent::__construct( $rules );
	}

	public function error( $field, $prefix = '', $suffix = '' ) {
		if ( ! empty($this->_field_data[$field])
		     && $this->_field_data[$field]['is_array'])
		{
			return ($prefix?$prefix:$this->_error_prefix)
			       .$this->_field_data[$field]['error']
			       .($suffix? $suffix:$this->_error_suffix);
		}

		return parent::error($field, $prefix, $suffix);
	}
}