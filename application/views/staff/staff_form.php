
<?php
$options = [

	0 => 'Sample1',
	1 => 'Sample2',
];
?>
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>STAFF</h3>
                </div>
                <div class='box-body'>
                    <?= form_open_multipart($action)?>
                        <table class='table table-bordered'>
                            <tr>
                                <td>NIP</td>
                                <td>
                                    <input <?= ($nip)? 'readonly':null?> type="text" class="form-control" name="nip" id="nip" placeholder="NIP" value="<?php echo $nip; ?>" />
			                        <?php echo form_error('nip') ?>
                                </td>
                            <tr>
                                <td>Nama Staff</td>
                                <td>
                                    <input type="text" class="form-control" name="nama_staff" id="nama_staff" placeholder="Nama Staff" value="<?php echo $nama_staff; ?>" />
									<?php echo form_error('nama_staff') ?>
                                </td>
                            <tr><td>Bagian</td>
                                <td>
                                    <input type="text" class="form-control" name="bagian" id="bagian" placeholder="Bagian" value="<?php echo $bagian; ?>" />
									<?php echo form_error('bagian') ?>
                                </td>
                            <tr><td>Foto</td>
                                <td>
                                    <img src="<?= get_image_staff($foto,250) ?>" alt="" class="img-thumbnail" width="100">
                                    <br>
                                    <?= form_upload('foto',$foto)?>
									<?php echo form_error('foto') ?>
                                </td>
                            <tr><td>Alamat</td>
                                <td>
                                    <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
									<?php echo form_error('alamat') ?>
                                </td></tr>
                            <tr><td>Username</td>
                                <td>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
									<?php echo form_error('username') ?>
                                </td>
                            <tr><td>Password</td>
                                <td>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
									<?php echo form_error('password') ?>
                                </td>
                            <tr><td>Hak Akses</td>
                                <td>
                                    <?php
                                    $akses = array(
	                                    '' => '-- Pilih Akses --',
	                                    'purchasing' => 'Purchasing',
	                                    'gudang' => 'Gudang',
	                                    'project_manager' => 'Project manager',
	                                    'admin' => 'Admin',
                                    );
                                    echo form_dropdown('hak_akses',$akses,$hak_akses,[
                                        'class' => 'form-control'
                                    ]);
                                    ?>
									<?php echo form_error('hak_akses') ?>
                                </td>
                                <input type="hidden" name="nip" value="<?php echo $nip; ?>" />
                            <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                    <a href="<?php echo site_url('staff') ?>" class="btn btn-default">Cancel</a></td></tr>

                        </table>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->