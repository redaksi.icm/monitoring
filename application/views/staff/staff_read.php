
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box'>
                <div class='box-header'>
                <h3 class='box-title'>Staff Read</h3>
        <table class="table table-bordered">
	    <tr><td width='200'>Nama Staff</td><td><?php echo $nama_staff; ?></td></tr>
	    <tr><td width='200'>Bagian</td><td><?php echo $bagian; ?></td></tr>
	    <tr><td width='200'>Foto</td><td><img src="<?= get_image_staff($foto,100) ?>" alt="" class="img-responsive" width="256"></td></tr>
	    <tr><td width='200'>Alamat</td><td><?php echo $alamat; ?></td></tr>
	    <tr><td width='200'>Username</td><td><?php echo $username; ?></td></tr>
	    <tr><td width='200'>Password</td><td><?php echo $password; ?></td></tr>
	    <tr><td width='200'>Hak Akses</td><td><?php echo $hak_akses; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('staff') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->