
<section class='content'>
  <div class='row'>
    <div class='col-xs-12'>
      <div class='box box-primary'>
        <div class='box-header'>
          <h3 class='box-title'>DETAIL_PP LIST</h3>
        </div>
        <div class='box-body'>
		<?php echo anchor('detail_pp/create/','Create',array('class'=>'btn btn-danger btn-flat btn-sm'));?>
		<?php echo anchor(site_url('detail_pp/pdf'), '<i class="fa fa-file-pdf-o"></i>', 'class="btn btn-social-icon btn-flat btn-sm"'); ?><div class='row'>
            <form action="<?php echo site_url('detail_pp/index'); ?>" class="form-inline" method="get">
				<div class='col-md-8'>				
					<div class ='form-group form-group-sm'>
						<label for='pwd'>Filter Berdasarkan: </label>
						<div class="input-group input-daterange">
							<input type="text" class="form-control form_date" name="dari" value="<?= date('Y-m-d')?>">
							<div class="input-group-addon">S/d</div>
							<input type="text" class="form-control to_date" name="sampai"  value="<?= date('Y-m-d')?>">
						</div>
					</div>
					<div class ='form-group form-group-sm'>
						<label for='pwd'>Status:</label>
						<select class="form-control select2">
							<option>Pilih Status...</option>
							<option selected value='1'>Aktif</option>
							<option value='0'>Tidak Aktif</option>
						</select>
					</div>
				</div>
				
				<div class='col-md-4'>
					<div class ='form-group form-group-sm pull-right'>												
						<div class="input-group">						
							<input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
							<span class="input-group-btn">								
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i></button>
							  <?php 
									if ($q <> '')
									{
										?>
										<a href="<?php echo site_url('detail_pp'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
										<?php
									}
								?>
							</span>
						</div>						
					</div>
				</div>				
            </form>            			
		</div>
		<br>
		<br>
        <div class='table-responsive'>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Pp</th>
		<th>Id Barang</th>
		<th>Permintaan</th>
		<th>Diterima</th>
		<th>Permintaan Satuan</th>
		<th>Diterima Satuan</th>
		<th>Action</th>
            </tr><?php
            foreach ($detail_pp_data as $detail_pp)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $detail_pp->id_pp ?></td>
			<td><?php echo $detail_pp->id_barang ?></td>
			<td><?php echo $detail_pp->permintaan ?></td>
			<td><?php echo $detail_pp->diterima ?></td>
			<td><?php echo $detail_pp->permintaan_satuan ?></td>
			<td><?php echo $detail_pp->diterima_satuan ?></td>
		    <td style="text-align:center" width="140px">
			<?php 
			echo anchor(site_url('detail_pp/read/'.$detail_pp->id_detail_pp),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-danger btn-xs')); 
			echo '  '; 
			echo anchor(site_url('detail_pp/update/'.$detail_pp->id_detail_pp),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-danger btn-xs')); 
			echo '  '; 
			echo anchor(site_url('detail_pp/delete/'.$detail_pp->id_detail_pp),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
		</tr>
                <?php
            }
            ?>
        </table>
		</div>
		<div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
			</div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>    

