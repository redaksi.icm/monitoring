<?php
/**
 * Created by PhpStorm.
 * Project:   monitoring.
 * File Name: dashboard.php.
 * User:      AcenetDev
 * Date:      7/11/2017
 * Time:      12:23 PM
 */
$hak_akses = $this->session->hak_akses;
?>
<section class="content">
	<div class="box">
		<div class="box-body">
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<?php
				switch ($hak_akses){
					case 'gudang':
						?>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Pengelolaan Barang</h4>
									<p>Halaman untuk mengelola data barang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('barang')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Barang Masuk</h4>
									<p>Halaman untuk mengelola data barang masuk</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('barang_masuk')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Barang Keluar</h4>
									<p>Halaman untuk mengelola data barang keluar</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('barang_keluar')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Order Barang (PP)</h4>
									<p>Halaman untuk mengelola mengorder barang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('pp')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<?php
						break;
					case 'purchasing':
						?>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Pengelolaan Subcount</h4>
									<p>Halaman untuk mengelola data subcount</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('subcount')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Pengelolaan Barang</h4>
									<p>Halaman untuk mengelola data barang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('barang')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Order Barang (PO)</h4>
									<p>Halaman untuk mengelola data order barang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('po')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Order Gudang (PP)</h4>
									<p>Halaman untuk mengelola data order gudang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('pp')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Proyek</h4>
									<p>Halaman untuk mengelola data proyek</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('proyek')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<?php
						break;
					case 'admin':
						?>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Pengolahan Data Staff</h4>
									<p>Halaman untuk mengelola data staff</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('staff')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<?php
						break;
					case 'project_manager':
						?>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Proyek</h4>
									<p>Halaman untuk mengelola data proyek</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('proyek')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Laporan Barang</h4>
									<p>Halaman untuk laporan barang</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('barang')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Laporan Subcount</h4>
									<p>Halaman untuk laporan subcount</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('subcount')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<!-- small box -->
							<div class="small-box bg-aqua">
								<div class="inner">
									<h4>Laporan Order Barang (PO)</h4>
									<p>Halaman untuk laporan subcount</p>
								</div>
								<a class="small-box-footer" href="<?= site_url('po')?>">Masuk <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<?php
						break;
				}
				?>

			</div>
			<!-- /.row -->
		</div>
	</div>


</section>
