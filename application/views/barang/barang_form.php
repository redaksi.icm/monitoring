
<?php
 $options = [
    0 => 'Sample1',
    1 => 'Sample2',
 ];
?>
        <!-- Main content -->
        <section class='content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='box box-primary'>
                <div class='box-header'>                
                  <h3 class='box-title'>Data Barang</h3>
                  </div>
                  <div class='box-body'>
                      
        <form action="<?php echo $action; ?>" method="post"><table class='table table-bordered'>
	    <tr><td>Nama Barang</td>
            <td>
            <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Nama Barang" value="<?php echo $nama_barang; ?>" />
            <?php echo form_error('nama_barang') ?>
        </td>
	    <tr><td>Satuan</td>
            <td>
            <input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan" value="<?php echo $satuan; ?>" />
            <?php echo form_error('satuan') ?>
        </td>
	    <input type="hidden" name="id_barang" value="<?php echo $id_barang; ?>" />
	    <tr><td colspan='2'><button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('barang') ?>" class="btn btn-default">Cancel</a></td></tr>
	
    </table>
    </form>
    </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->