<?php $hak_akses = $this->session->hak_akses;?>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Barang</h3>
                </div>
                <div class='box-body'>
                    <div class='row'>
                        <form action="<?php echo site_url('barang/index'); ?>" class="form-inline" method="get">
                            <div class="col-md-8">
								<?php echo ($hak_akses != 'project_manager') ? anchor('barang/create/','Tambah Barang',array('class'=>'btn btn-danger btn-flat btn-sm')):null;?>
								<?php echo anchor('barang/pdf/','<i class="fa fa-print"></i>',array('class'=>'btn btn-default btn-flat btn-sm', 'target' => '_blank'));?>
                            </div>
                            <div class='col-md-4'>
                                <div class ='form-group form-group-sm pull-right'>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>" placeholder="">
                                        <span class="input-group-btn">
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i> Cari</button>
											<?php
											if ($q <> '')
											{
												?>
                                                <a href="<?php echo site_url('barang'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
												<?php
											}
											?>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Satuan</th>
                                <th>Action</th>
                            </tr><?php
							foreach ($barang_data as $barang)
							{
								?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $barang->nama_barang ?></td>
                                    <td><?php echo $barang->satuan ?></td>
                                    <td style="text-align:center" width="140px">
										<?php
										echo anchor(site_url('barang/read/'.$barang->id_barang),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-primary btn-xs'));
										echo '  ';
										echo ($hak_akses != 'project_manager') ? anchor(site_url('barang/update/'.$barang->id_barang),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-xs')):null;

										?>
                                    </td>
                                </tr>
								<?php
							}
							?>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
							<?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

