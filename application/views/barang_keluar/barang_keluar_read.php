
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Barang_keluar Read</h3>
                    <table class="table table-bordered">
                        <tr><td width='200'>Proyek</td><td><?php echo get_proyek_name($id_proyek); ?></td></tr>
                        <tr><td width='200'>Tanggal</td><td><?php echo ymdToDmy($tanggal); ?></td></tr>
                        <tr><td width='200'>Keterangan</td><td><?php echo $keterangan; ?></td></tr>
                        <tr><td></td><td><a href="<?php echo site_url('barang_keluar') ?>" class="btn btn-default">Cancel</a></td></tr>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Barang Keluar</h3>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						$detail = $this->db->query("SELECT
A.id_detail_keluar,
A.id_barang,
A.jumlah,
A.satuan,
B.nama_barang
FROM
detail_keluar AS A
INNER JOIN barang AS B ON A.id_barang = B.id_barang
WHERE
A.id_keluar = $id_keluar")->result();
						if($detail){
							$i=0;
							$tPermintaan= 0;
							foreach ($detail as $row){

								?>
                                <tr>
                                    <td><?= $i+=1?></td>
                                    <td><?= $row->nama_barang?></td>
                                    <td class="text-right"><?= $row->jumlah?></td>
                                    <td><?= $row->satuan?></td>
                                </tr>
								<?php
								$tPermintaan += $row->jumlah;
							}
						}
						?>
                        <tfoot style="font-weight: bold;">
                        <tr>
                            <td colspan="2" class="text-center">Total</td>
                            <td  class="text-right"><?= $tPermintaan?></td>
                            <td></td>
                        </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->