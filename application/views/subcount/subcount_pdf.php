<!doctype html>
<html>
<head>
    <title>harviacode.com - codeigniter crud generator</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <style>
        .word-table {
            border:1px solid black !important;
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<center><h2>Laporan Subcount</h2></center>
<table class="word-table" style="margin-bottom: 10px">
    <tr>
        <th>No</th>
        <th>Nama Subcount</th>
        <th>Alamat</th>
        <th>No Telp</th>

    </tr><?php
	foreach ($subcount_data as $subcount)
	{
		?>
        <tr>
            <td><?php echo ++$start ?></td>
            <td><?php echo $subcount->nama_subcount ?></td>
            <td><?php echo $subcount->alamat ?></td>
            <td><?php echo $subcount->no_telp ?></td>
        </tr>
		<?php
	}
	?>
</table>
</body>
</html>