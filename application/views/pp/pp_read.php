
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Detail PP</h3>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr><td width='200'>No Pp</td><td><?php echo $no_pp; ?></td></tr>
                        <tr><td width='200'>Proyek</td><td><?php echo $id_proyek; ?></td></tr>
                        <tr><td width='200'>Gudang</td><td><?php echo $id_gudang; ?></td></tr>
                        <tr><td width='200'>Tanggal Pp</td><td><?php echo ymdToDmy($tanggal_pp); ?></td></tr>
                        <tr><td></td><td><a href="<?php echo site_url('pp') ?>" class="btn btn-default">Cancel</a></td></tr>
                    </table>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Permintaan</h3>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Permintaan</th>
                            <th>Satuan</th>
                            <th>Diterima</th>
                            <th>Satuan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $detail = $this->db->query("SELECT
A.id_detail_pp,
A.id_barang,
B.nama_barang,
A.permintaan,
A.diterima,
A.permintaan_satuan,
A.diterima_satuan
FROM
detail_pp AS A
INNER JOIN barang AS B ON A.id_barang = B.id_barang WHERE
A.id_pp = $id_pp")->result();
                        if($detail){
                            $i=0;
	                        $tPermintaan= 0;
	                        $tDiterima= 0;
                            foreach ($detail as $row){

                                ?>
                                <tr>
                                    <td><?= $i+=1?></td>
                                    <td><?= $row->nama_barang?></td>
                                    <td class="text-right"><?= $row->permintaan?></td>
                                    <td><?= $row->permintaan_satuan?></td>
                                    <td class="text-right"><?= $row->diterima?></td>
                                    <td><?= $row->diterima_satuan?></td>
                                </tr>
                                <?php
                                $tPermintaan += $row->permintaan;
	                            $tDiterima += $row->diterima;
                            }
                        }
                        ?>
                        </tbody>
                        <tfoot style="font-weight: bold;">
                            <tr>
                                <td colspan="2" class="text-center">Total</td>
                                <td  class="text-right"><?= $tPermintaan?></td>
                                <td></td>
                                <td  class="text-right"><?= $tDiterima?></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->