
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box box-primary'>
                <div class='box-header'>
                    <h3 class='box-title'>PP LIST</h3>
                </div>
                <div class='box-body'>
					<?php echo anchor('pp/create/','<i class="fa fa-plus"></i> Buat PP Baru',array('class'=>'btn btn-danger btn-flat btn-sm'));?>
                    <div class='row'>
                        <form action="<?php echo site_url('pp/index'); ?>" class="form-inline" method="get">
                            <div class='col-md-8'>
                                <div class ='form-group form-group-sm'>
                                    <label for='pwd'>Filter Berdasarkan: </label>
                                    <div class="input-group input-daterange">
                                        <input type="text" class="form-control form_date" name="dari" value="<?= $dari?>">
                                        <div class="input-group-addon">S/d</div>
                                        <input type="text" class="form-control to_date" name="sampai"  value="<?= $sampai?>">
                                    </div>
                                </div>
                            </div>
                            <div class='col-md-4 pull-right'>
                                <div class ='form-group form-group-sm pull-right'>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                        <span class="input-group-btn">
							  <button class="btn btn-sm btn-flat btn-primary" type="submit"><i class='fa fa-search'></i></button>
											<?php
											if ($q <> '')
											{
												?>
                                                <a href="<?php echo site_url('pp'); ?>" class="btn btn-sm btn-flat btn-default"><i class='fa fa-minus-square-o'></i>&nbsp;Reset</a>
												<?php
											}
											?>
							</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br>
                    <br>
                    <div class='table-responsive'>
                        <table class="table table-bordered" style="margin-bottom: 10px">
                            <tr>
                                <th>No</th>
                                <th>No Pp</th>
                                <th>Proyek</th>
                                <th>Gudang</th>
                                <th>Tanggal Pp</th>
                                <th>Action</th>
                            </tr><?php
							foreach ($pp_data as $pp)
							{
								?>
                                <tr>
                                    <td width="80px"><?php echo ++$start ?></td>
                                    <td><?php echo $pp->no_pp ?></td>
                                    <td><?php echo get_proyek($pp->id_proyek)  ?></td>
                                    <td><?php echo get_gudang($pp->id_gudang) ?></td>
                                    <td><?php echo date('d-m-Y',strtotime($pp->tanggal_pp)) ?></td>
                                    <td style="text-align:center" width="140px">
										<?php
										echo anchor(site_url('pp/read/'.$pp->id_pp),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-primary btn-xs'));
										echo '  ';
										echo anchor(site_url('pp/update/'.$pp->id_pp),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-xs'));
										echo '  ';
										//echo anchor(site_url('pp/delete/'.$pp->id_pp),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-xs" onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
										?>
                                    </td>
                                </tr>
								<?php
							}
							?>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary btn-flat">Total Record : <?php echo $total_rows ?></a>
                        </div>
                        <div class="col-md-6 text-right">
							<?php echo $pagination ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

