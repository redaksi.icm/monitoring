<!doctype html>
<html>
<head>
    <title>harviacode.com - codeigniter crud generator</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
    <style>
        .word-table {
            border:1px solid black !important;
            border-collapse: collapse !important;
            width: 100%;
        }
        .word-table tr th, .word-table tr td{
            border:1px solid black !important;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<center><h2>Laporan Barang_masuk</h2></center>
<table class="word-table" style="margin-bottom: 10px">
    <tr>
        <th>No</th>
        <th>No Po</th>
        <th>Subcount</th>
        <th>Tanggal</th>
<!--        <th>Nip</th>-->

    </tr><?php
	foreach ($barang_masuk_data as $barang_masuk)
	{
		?>
        <tr>
            <td><?php echo ++$start ?></td>
            <td><?php echo $barang_masuk->no_po ?></td>
            <td><?php echo get_subcount($barang_masuk->id_subcount) ?></td>
            <td><?php echo ymdToDmy($barang_masuk->tanggal) ?></td>
<!--            <td>--><?php //echo $barang_masuk->nip ?><!--</td>-->
        </tr>
		<?php
	}
	?>
</table>
</body>
</html>