
<!-- Main content -->
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Barang Masuk</h3>
                    <table class="table table-bordered">
                        <tr><td width='200'>No Po</td><td><?php echo $no_po; ?></td></tr>
                        <tr><td width='200'>Subcount</td><td><?php echo get_subcount_name($id_subcount); ?></td></tr>
                        <tr><td width='200'>Tanggal</td><td><?php echo ymdToDmy($tanggal); ?></td></tr>
                        <tr><td></td><td><a href="<?php echo site_url('barang_masuk') ?>" class="btn btn-default">Cancel</a></td></tr>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Daftar Barang Masuk</h3>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Satuan</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $detail = $this->db->query("SELECT
A.id_detail_masuk,
A.id_barang,
A.jumlah,
A.satuan,
B.nama_barang
FROM
detail_masuk AS A
INNER JOIN barang AS B ON A.id_barang = B.id_barang
WHERE
A.id_masuk = $id_masuk")->result();
                        if($detail){
	                        $i=0;
	                        $tPermintaan= 0;
	                        foreach ($detail as $row){

		                        ?>
                                <tr>
                                    <td><?= $i+=1?></td>
                                    <td><?= $row->nama_barang?></td>
                                    <td class="text-right"><?= $row->jumlah?></td>
                                    <td><?= $row->satuan?></td>
                                </tr>
		                        <?php
		                        $tPermintaan += $row->jumlah;
	                        }
                        }
                        ?>
                        <tfoot style="font-weight: bold;">
                        <tr>
                            <td colspan="2" class="text-center">Total</td>
                            <td  class="text-right"><?= $tPermintaan?></td>
                            <td></td>
                        </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
