<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Staff extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Staff_model');
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'staff/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'staff/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'staff/';
            $config['first_url'] = base_url() . 'staff/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Staff_model->total_rows($q);
        $staff = $this->Staff_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'staff_data' => $staff,
            'c_header' => 'Staff',
            'c_sub_header' => 'Daftar Staff',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','staff/staff_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Staff_model->get_by_id($id);
        if ($row) {
            $data = array(
		'nip' => $row->nip,
		'nama_staff' => $row->nama_staff,
		'bagian' => $row->bagian,
		'foto' => $row->foto,
		'alamat' => $row->alamat,
		'username' => $row->username,
		'password' => $row->password,
		'hak_akses' => $row->hak_akses,
	    );
            $this->template->load('template','staff/staff_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('staff'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('staff/create_action'),
            'is_newRecord' => true,
	    'nip' => set_value('nip'),
	    'nama_staff' => set_value('nama_staff'),
	    'bagian' => set_value('bagian'),
	    'foto' => set_value('foto'),
	    'alamat' => set_value('alamat'),
	    'username' => set_value('username'),
	    'password' => set_value('password'),
	    'hak_akses' => set_value('hak_akses'),
	);
        $this->template->load('template','staff/staff_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_staff' => $this->input->post('nama_staff',TRUE),
		'bagian' => $this->input->post('bagian',TRUE),
//		'foto' => $this->input->post('foto',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'hak_akses' => $this->input->post('hak_akses',TRUE),
	    );
	        $upload = upload_image('foto',FCPATH. 'assets/media/image','staff'.'_'.time(),'staff');
	        if($upload['status'] == true){
		        $data['foto'] = $upload['image_name'];
	        }

            $this->Staff_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('staff'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Staff_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('staff/update_action'),
                'is_newRecord' => false,
		'nip' => set_value('nip', $row->nip),
		'nama_staff' => set_value('nama_staff', $row->nama_staff),
		'bagian' => set_value('bagian', $row->bagian),
		'foto' => set_value('foto', $row->foto),
		'alamat' => set_value('alamat', $row->alamat),
		'username' => set_value('username', $row->username),
		'password' => set_value('password', $row->password),
		'hak_akses' => set_value('hak_akses', $row->hak_akses),
	    );
            $this->template->load('template','staff/staff_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('staff'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('nip', TRUE));
        } else {
            $data = array(
		'nama_staff' => $this->input->post('nama_staff',TRUE),
		'bagian' => $this->input->post('bagian',TRUE),
//		'foto' => $this->input->post('foto',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'username' => $this->input->post('username',TRUE),
		'password' => $this->input->post('password',TRUE),
		'hak_akses' => $this->input->post('hak_akses',TRUE),
	    );
	        $upload = upload_image('foto',FCPATH. 'assets/media/image','staff'.'_'.time(),'staff');
	        if($upload['status'] == true){
		        $data['foto'] = $upload['image_name'];
	        }

            $this->Staff_model->update($this->input->post('nip', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('staff'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Staff_model->get_by_id($id);

        if ($row) {
            $this->Staff_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('staff'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('staff'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nip', 'nip', 'trim|required|is_unique[staff.nip]');
	$this->form_validation->set_rules('nama_staff', 'nama staff', 'trim|required');
	$this->form_validation->set_rules('bagian', 'bagian', 'trim|required');
//	$this->form_validation->set_rules('foto', 'foto', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('hak_akses', 'hak akses', 'trim|required');

	$this->form_validation->set_rules('nip', 'nip', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'staff_data' => $this->Staff_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('staff/staff_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('staff.pdf', 'D'); 
    }

}

/* End of file Staff.php */
/* Location: ./application/controllers/Staff.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */