<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gudang extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gudang_model');
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'gudang/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'gudang/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'gudang/';
            $config['first_url'] = base_url() . 'gudang/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Gudang_model->total_rows($q);
        $gudang = $this->Gudang_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'gudang_data' => $gudang,
            'c_header' => 'Gudang',
            'c_sub_header' => 'Daftar Gudang',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','gudang/gudang_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Gudang_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_gudang' => $row->id_gudang,
		'nama_gudang' => $row->nama_gudang,
		'alamat' => $row->alamat,
		'no_telp' => $row->no_telp,
	    );
            $this->template->load('template','gudang/gudang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gudang'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('gudang/create_action'),
            'is_newRecord' => true,
	    'id_gudang' => set_value('id_gudang'),
	    'nama_gudang' => set_value('nama_gudang'),
	    'alamat' => set_value('alamat'),
	    'no_telp' => set_value('no_telp'),
	);
        $this->template->load('template','gudang/gudang_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_gudang' => $this->input->post('nama_gudang',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Gudang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('gudang'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Gudang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('gudang/update_action'),
                'is_newRecord' => false,
		'id_gudang' => set_value('id_gudang', $row->id_gudang),
		'nama_gudang' => set_value('nama_gudang', $row->nama_gudang),
		'alamat' => set_value('alamat', $row->alamat),
		'no_telp' => set_value('no_telp', $row->no_telp),
	    );
            $this->template->load('template','gudang/gudang_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gudang'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_gudang', TRUE));
        } else {
            $data = array(
		'nama_gudang' => $this->input->post('nama_gudang',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Gudang_model->update($this->input->post('id_gudang', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('gudang'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Gudang_model->get_by_id($id);

        if ($row) {
            $this->Gudang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('gudang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('gudang'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_gudang', 'nama gudang', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('no_telp', 'no telp', 'trim|required');

	$this->form_validation->set_rules('id_gudang', 'id_gudang', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'gudang_data' => $this->Gudang_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('gudang/gudang_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('gudang.pdf', 'D'); 
    }

}

/* End of file Gudang.php */
/* Location: ./application/controllers/Gudang.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */