<?php

/**
 * Created by PhpStorm.
 * Project:   monitoring.
 * File Name: Laporan.php.
 * User:      AcenetDev
 * Date:      8/5/2017
 * Time:      12:39 AM
 */
class Laporan extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Barang_model');
		$this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
			<script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
			</script>
			<?php
		}
	}

	public function subcount() {
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));

		if ($q <> '') {
			$config['base_url'] = site_url('laporan/subcount/?q=' . urlencode($q));
			$config['first_url'] = base_url('laporan/subcount/?q=' . urlencode($q));
		} else {
			$config['base_url'] = base_url('laporan/subcount/');
			$config['first_url'] = base_url('laporan/subcount/');
		}
		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Subcount_model->total_rows($q);
		$subcount = $this->Subcount_model->get_limit_data($config['per_page'], $start, $q);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'subcount_data' => $subcount,
			'c_header' => 'Subcount',
			'c_sub_header' => 'Daftar Subcount',
			'q' => $q,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->template->load('template','laporan/subcount_list', $data);
	}


}