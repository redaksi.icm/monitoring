<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Barang_keluar extends CI_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model('Barang_keluar_model');
		$this->load->model('Detail_keluar_model');
		$this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
            <script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
            </script>
			<?php
		}
	}

	public function index()
	{
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));
		$dari = intval($this->input->get('dari'));
		$sampai = intval($this->input->get('sampai'));

		$data_array = [
			'dari' => $dari,
			'sampai' => $sampai,
			'q' => $q,
			'start' => $start,
		];

		$link = http_build_query($data_array);
		$config['base_url'] = site_url('barang_keluar/?' . $link);
		$config['first_url'] = site_url('barang_keluar/?' . $link);

		if ($q == '' && $dari && $sampai) {
			$config['base_url'] = site_url('barang_keluar/');
			$config['first_url'] = site_url('barang_keluar/');
		}
		$data_array = [
			'dari' => ($dari)?dmyToYmd($dari):null,
			'sampai' => ($sampai)?dmyToYmd($sampai):null,
			'q' => $q,
			'start' => $start,
		];

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->Barang_keluar_model->total_rows($data_array);
		$barang_keluar = $this->Barang_keluar_model->get_limit_data($config['per_page'], $start, $data_array);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'barang_keluar_data' => $barang_keluar,
			'c_header' => 'Barang keluar',
			'c_sub_header' => 'Daftar Barang keluar',
			'q' => $q,
			'dari' => $dari,
			'sampai' => $sampai,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->template->load('template','barang_keluar/barang_keluar_list', $data);
	}

	public function read($id)
	{
		$row = $this->Barang_keluar_model->get_by_id($id);
		if ($row) {
			$data = array(
				'c_header' => 'Barang keluar',
				'c_sub_header' => 'Detail Barang keluar',
				'id_keluar' => $row->id_keluar,
				'id_proyek' => $row->id_proyek,
				'tanggal' => $row->tanggal,
				'keterangan' => $row->keterangan,
			);
			$this->template->load('template','barang_keluar/barang_keluar_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_keluar'));
		}
	}

	public function create()
	{
		$data = array(
			'c_header' => 'Barang keluar',
			'c_sub_header' => 'Tambah Barang keluar',
			'button' => 'Create',
			'action' => site_url('barang_keluar/create_action'),
			'is_newRecord' => true,
			'id_keluar' => set_value('id_keluar'),
			'id_proyek' => set_value('id_proyek'),
			'tanggal' => set_value('tanggal'),
			'keterangan' => set_value('keterangan'),
			'detail_bm' => null,
		);
		$this->template->load('template','barang_keluar/barang_keluar_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$id_user = $this->session->userdata('nip');
			$data = array(
				'id_proyek' => $this->input->post('id_proyek',TRUE),
				'tanggal' => dmyToYmd($this->input->post('tanggal',TRUE)),
				'keterangan' => $this->input->post('keterangan',TRUE),
				'nip' => $id_user
			);

			$id_bm = $this->Barang_keluar_model->insert($data);
			extract($_POST);
			$i = 0;
			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_keluar' => $id_bm,
					'id_barang' => $value,
					'jumlah' => $jumlah[$i],
//	                'diterima' => $diterima[$i],
					'satuan' => $satuan[$i],
//	                'diterima_satuan' => $diterima_satuan[$i],
				];
				$this->Detail_keluar_model->insert($detailPP);
				$i++;
			}
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url("barang_keluar/read/$id_bm"));
		}
	}

	public function update($id)
	{
		$row = $this->Barang_keluar_model->get_by_id($id);

		if ($row) {
			$detailBM = $this->Detail_keluar_model->get_by_id_bk($id);
			$data = array(
				'c_header' => 'Barang keluar',
				'c_sub_header' => 'Ubah Barang keluar',
				'button' => 'Update',
				'action' => site_url('barang_keluar/update_action'),
				'is_newRecord' => false,
				'id_keluar' => set_value('id_keluar', $row->id_keluar),
				'id_proyek' => set_value('id_proyek', $row->id_proyek),
				'tanggal' => set_value('tanggal', ymdToDmy($row->tanggal)),
				'keterangan' => set_value('keterangan', $row->keterangan),
				'detail_bm' => $detailBM,
			);
			$this->template->load('template','barang_keluar/barang_keluar_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_keluar'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id_keluar', TRUE));
		} else {
			$id_keluar = $this->input->post('id_keluar', TRUE);
			$data = array(
				'id_proyek' => $this->input->post('id_proyek',TRUE),
				'tanggal' => dmyToYmd($this->input->post('tanggal',TRUE)),
				'keterangan' => $this->input->post('keterangan',TRUE),
			);

			$this->Barang_keluar_model->update($this->input->post('id_keluar', TRUE), $data);
			extract($_POST);
			$i = 0;
			$this->Detail_keluar_model->delete_bybk($id_keluar);

			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_masuk' => $id_masuk,
					'id_barang' => $value,
					'jumlah' => ($jumlah[$i])? $jumlah[$i]:0,
					'satuan' => ($satuan[$i])? $satuan[$i]:0,
//					'diterima' => ($diterima[$i])? $diterima[$i]:null,
//					'permintaan_satuan' => ($satuan[$i])? $satuan[$i]:null,
//					'diterima_satuan' => ($diterima_satuan[$i])? $diterima_satuan[$i]:null,
				];

				$i++;
				$this->Detail_keluar_model->insert($detailPP);
			}

			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('barang_keluar'));
		}
	}

	public function delete($id)
	{
		$row = $this->Barang_keluar_model->get_by_id($id);

		if ($row) {
			$this->Barang_keluar_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('barang_keluar'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('barang_keluar'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('id_proyek', 'id proyek', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

		$this->form_validation->set_rules('id_keluar', 'id_keluar', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	function pdf()
	{
		$data = array(
			'barang_keluar_data' => $this->Barang_keluar_model->get_all(),
			'start' => 0
		);

		ini_set('memory_limit', '32M');
//		$html = $this->load->view('barang_keluar/barang_keluar_pdf', $data, true);
		$this->load->view('barang_keluar/barang_keluar_pdf', $data);
//		$this->load->library('pdf');
//		$pdf = $this->pdf->load();
//		$pdf->WriteHTML($html);
//		$pdf->Output('barang_keluar.pdf', 'D');
	}

	public function add_row_bk() {
		?>
        <tr>
            <td>
				<?php
				$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
				$list_barang[''] = '-- Pilih Barang --';
				if($list_id_barang){
					foreach ($list_id_barang as $dia){
						$list_barang[$dia->id_barang] = $dia->nama_barang;
					}
				}
				?>
				<?= form_dropdown('id_barang[]',$list_barang,null,[
					'class'=> 'form-control select2'
				])?>
            </td>
            <td>
                <input type="text" class="form-control" name="jumlah[]">
            </td>
            <td>
                <input type="hidden"class="form-control" name="id_detail_keluar[]">
                <input type="text"class="form-control" name="satuan[]">
            </td>
            <td>
                <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
            </td>
        </tr>
		<?php
	}

}

/* End of file Barang_keluar.php */
/* Location: ./application/controllers/Barang_keluar.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-11 09:26:30 */
/* Modification By Rusli */
/* http://harviacode.com */