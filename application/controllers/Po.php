<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Po extends CI_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model('Po_model');
		$this->load->model('Detail_po_model');
		$this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		} elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
            <script>
                alert('Silahkan Login Terlebih Dahulu!');
                window.location.href = "<?=site_url('auth/login')?>";
            </script>
			<?php
		}
	}

	public function index()
	{
		$hak_akses = $this->session->hak_akses;
		$q = urldecode($this->input->get('q', TRUE));
		$start = intval($this->input->get('start'));
		$dari = intval($this->input->get('dari'));
		$sampai = intval($this->input->get('sampai'));

		$data_array = [
			'dari' => $dari,
			'sampai' => $sampai,
			'q' => $q,
			'start' => $start,
		];

		$link = http_build_query($data_array);

		$config['base_url'] = site_url('po/?' . $link);
		$config['first_url'] = site_url('po/?' . $link);

		if ($q == '' && $dari && $sampai) {
			$config['base_url'] = site_url('po/') ;
			$config['first_url'] = site_url('po/');
		}

		$config['per_page'] = 10;
		$config['page_query_string'] = TRUE;

		$config['total_rows'] = $this->Po_model->total_rows($data_array);
		$po = $this->Po_model->get_limit_data($config['per_page'], $start, $data_array);

		$this->load->library('pagination');
		$this->pagination->initialize($config);

		$data = array(
			'po_data' => $po,
			'c_header' => ($hak_akses == 'project_manager')? 'Laporan Purchasing':'Purchasing',
			'c_sub_header' => 'Daftar PO',
			'q' => $q,
			'dari' => $dari,
			'sampai' => $sampai,
			'pagination' => $this->pagination->create_links(),
			'total_rows' => $config['total_rows'],
			'start' => $start,
		);
		$this->template->load('template','po/po_list', $data);
	}

	public function read($id)
	{
		$row = $this->Po_model->get_by_id($id);
		if ($row) {
			$data = array(
				'id_po' => $row->id_po,
				'no_po' => $row->no_po,
				'id_subcount' => $row->id_subcount,
				'id_proyek' => $row->id_proyek,
			);
			$this->template->load('template','po/po_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('po'));
		}
	}

	public function create()
	{
		$data = array(
			'c_header' => 'Purchasing',
			'c_sub_header' => 'Tambah PO',
			'button' => 'Create',
			'action' => site_url('po/create_action'),
			'is_newRecord' => true,
			'id_po' => set_value('id_po'),
			'no_po' => set_value('no_po'),
			'id_subcount' => set_value('id_subcount'),
			'id_proyek' => set_value('id_proyek'),
			'detail_pp' => null,
		);
		$this->template->load('template','po/po_form', $data);
	}

	public function create_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'no_po' => $this->input->post('no_po',TRUE),
				'id_subcount' => $this->input->post('id_subcount',TRUE),
				'id_proyek' => $this->input->post('id_proyek',TRUE),
			);

			$idpp = $this->Po_model->insert($data);
			extract($_POST);
			$i = 0;
			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_pp' => $idpp,
					'id_barang' => $value,
					'jumlah' => $jumlah[$i],
//	                'diterima' => $diterima[$i],
					'satuan' => $satuan[$i],
//	                'diterima_satuan' => $diterima_satuan[$i],
				];
				$this->Detail_po_model->insert($detailPP);
				$i++;
			}
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('po'));
		}
	}

	public function update($id)
	{
		$row = $this->Po_model->get_by_id($id);

		if ($row) {
			$detailPO = $this->Detail_po_model->get_by_id_po($id);
			$data = array(
				'c_header' => 'Purchasing',
				'c_sub_header' => 'Update PO',
				'button' => 'Update',
				'action' => site_url('po/update_action'),
				'is_newRecord' => false,
				'id_po' => set_value('id_po', $row->id_po),
				'no_po' => set_value('no_po', $row->no_po),
				'id_subcount' => set_value('id_subcount', $row->id_subcount),
				'id_proyek' => set_value('id_proyek', $row->id_proyek),
				'detail_pp' => $detailPP,
			);
			$this->template->load('template','po/po_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('po'));
		}
	}

	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id_po', TRUE));
		} else {
			$id_pp = $this->input->post('id_po', TRUE);
			$data = array(
				'no_po' => $this->input->post('no_po',TRUE),
				'id_subcount' => $this->input->post('id_subcount',TRUE),
				'id_proyek' => $this->input->post('id_proyek',TRUE),
			);

			$this->Pp_model->update($id_pp, $data);
			extract($_POST);
			$i = 0;
			$this->Detail_po_model->delete_bypo($id_pp);
			foreach ($id_barang as $key => $value){
				$detailPP = [
					'id_po' => $id_pp,
					'id_barang' => $value,
					'jumlah' => ($jumlah[$i])? $jumlah[$i]:0,
					'satuan' => ($satuan[$i])? $satuan[$i]:null,
					'keluar' => ($keluar[$i])? $keluar[$i]:null,
					'sisa' => ($sisa[$i])? $sisa[$i]:null,
				];

				$i++;
				$this->Detail_po_model->insert($detailPP);
			}

			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('po'));
		}
	}

	public function delete($id)
	{
		$row = $this->Po_model->get_by_id($id);

		if ($row) {
			$this->Po_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('po'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('po'));
		}
	}

	public function _rules()
	{
		$this->form_validation->set_rules('no_po', 'no po', 'trim|required');
		$this->form_validation->set_rules('id_subcount', 'id subcount', 'trim|required');
		$this->form_validation->set_rules('id_proyek', 'id proyek', 'trim|required');

		$this->form_validation->set_rules('id_po', 'id_po', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	function pdf()
	{
		$data = array(
			'po_data' => $this->Po_model->get_all(),
			'start' => 0
		);

		ini_set('memory_limit', '32M');
//		$html = $this->load->view('po/po_pdf', $data, true);
		$this->load->view('po/po_pdf', $data);
//		$this->load->library('pdf');
//		$pdf = $this->pdf->load();
//		$pdf->WriteHTML($html);
//		$pdf->Output('po.pdf', 'D');
	}

	public function add_row_pp() {
		?>
        <tr>
            <td>
				<?php
				$list_id_barang = $this->db->query("SELECT * FROM barang")->result();
				$list_barang[''] = '-- Pilih Barang --';
				if($list_id_barang){
					foreach ($list_id_barang as $dia){
						$list_barang[$dia->id_barang] = $dia->nama_barang;
					}
				}
				?>
				<?= form_dropdown('id_barang[]',$list_barang,null,[
					'class'=> 'form-control select2'
				])?>
            </td>
            <td>
                <input type="text" class="form-control" name="jumlah[]">
            </td>
            <td>
                <input type="hidden"class="form-control" name="id_detail_po[]">
                <input type="text"class="form-control" name="satuan[]">
                <input type="hidden"class="form-control" name="keluar[]">
                <input type="hidden"class="form-control" name="sisa[]">
            </td>
            <td>
                <button type="button" class="tambah btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i></button>
                <button type="button" class="hapus btn btn-danger btn-sm btn-flat"><i class="fa fa-minus"></i></button>
            </td>
        </tr>
		<?php
	}

}

/* End of file Po.php */
/* Location: ./application/controllers/Po.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */