<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proyek extends CI_Controller
{
    
        
    function __construct()
    {
        parent::__construct();
        $this->load->model('Proyek_model');
        $this->load->library('form_validation');
		if($this->session->userdata('logged_in')){
            $session_data = $this->session->userdata('logged_in');            
        } elseif($this->session->userdata('logged_in') == ''){
			$this->session->unset_userdata('logged_in');
			$this->session->sess_destroy();
			?>
                    <script>
                        alert('Silahkan Login Terlebih Dahulu!');
                        window.location.href = "<?=site_url('auth/login')?>";
                    </script>
            <?php
		}
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'proyek/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'proyek/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'proyek/';
            $config['first_url'] = base_url() . 'proyek/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Proyek_model->total_rows($q);
        $proyek = $this->Proyek_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'proyek_data' => $proyek,
            'c_header' => 'Proyek',
            'c_sub_header' => 'Daftar Proyek',
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','proyek/proyek_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Proyek_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_proyek' => $row->id_proyek,
		'nama_proyek' => $row->nama_proyek,
		'alamat_proyek' => $row->alamat_proyek,
		'tahun_proyek' => $row->tahun_proyek,
	    );
            $this->template->load('template','proyek/proyek_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('proyek'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('proyek/create_action'),
            'is_newRecord' => true,
	    'id_proyek' => set_value('id_proyek'),
	    'nama_proyek' => set_value('nama_proyek'),
	    'alamat_proyek' => set_value('alamat_proyek'),
	    'tahun_proyek' => set_value('tahun_proyek'),
	);
        $this->template->load('template','proyek/proyek_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_proyek' => $this->input->post('nama_proyek',TRUE),
		'alamat_proyek' => $this->input->post('alamat_proyek',TRUE),
		'tahun_proyek' => $this->input->post('tahun_proyek',TRUE),
	    );

            $this->Proyek_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('proyek'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Proyek_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('proyek/update_action'),
                'is_newRecord' => false,
		'id_proyek' => set_value('id_proyek', $row->id_proyek),
		'nama_proyek' => set_value('nama_proyek', $row->nama_proyek),
		'alamat_proyek' => set_value('alamat_proyek', $row->alamat_proyek),
		'tahun_proyek' => set_value('tahun_proyek', $row->tahun_proyek),
	    );
            $this->template->load('template','proyek/proyek_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('proyek'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_proyek', TRUE));
        } else {
            $data = array(
		'nama_proyek' => $this->input->post('nama_proyek',TRUE),
		'alamat_proyek' => $this->input->post('alamat_proyek',TRUE),
		'tahun_proyek' => $this->input->post('tahun_proyek',TRUE),
	    );

            $this->Proyek_model->update($this->input->post('id_proyek', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('proyek'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Proyek_model->get_by_id($id);

        if ($row) {
            $this->Proyek_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('proyek'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('proyek'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_proyek', 'nama proyek', 'trim|required');
	$this->form_validation->set_rules('alamat_proyek', 'alamat proyek', 'trim|required');
	$this->form_validation->set_rules('tahun_proyek', 'tahun proyek', 'trim|required');

	$this->form_validation->set_rules('id_proyek', 'id_proyek', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    function pdf()
    {
        $data = array(
            'proyek_data' => $this->Proyek_model->get_all(),
            'start' => 0
        );
        
        ini_set('memory_limit', '32M');
        $html = $this->load->view('proyek/proyek_pdf', $data, true);
        $this->load->library('pdf');
        $pdf = $this->pdf->load();
        $pdf->WriteHTML($html);
        $pdf->Output('proyek.pdf', 'D'); 
    }

}

/* End of file Proyek.php */
/* Location: ./application/controllers/Proyek.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-07-02 16:46:00 */
/* Modification By Rusli */
/* http://harviacode.com */