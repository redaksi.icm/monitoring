/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50624
Source Host           : 127.0.0.1:3306
Source Database       : monitoring_gudang

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-08-05 02:35:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(255) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of barang
-- ----------------------------
INSERT INTO `barang` VALUES ('1', '+ Kaca polos 6 mm + sealant', null);
INSERT INTO `barang` VALUES ('2', '+ Ongkos pasang kusen aluminium', null);
INSERT INTO `barang` VALUES ('3', '1 gang 4 kawat kotak kontak telepon dengan penutup, putih (KB31TS)', null);
INSERT INTO `barang` VALUES ('4', '1 gang 4 kawat kotak kontak telepon dengan penutup, putih (KB31TS)', null);
INSERT INTO `barang` VALUES ('5', '1 gang 4 kawat kotak kontak telepon dengan penutup, putih (KB31TS)', null);
INSERT INTO `barang` VALUES ('6', '1 gang kotak kontak data cat 5E dengan penutup (KB31RI5E)', null);
INSERT INTO `barang` VALUES ('7', '1 gang kotak kontak data Cat 5E dengan penutup (KB31RJ5E)', null);
INSERT INTO `barang` VALUES ('8', '1 panel untuk 1 pompa', null);
INSERT INTO `barang` VALUES ('9', '1 unit daun pintu sliding uk. 253 x 101', null);
INSERT INTO `barang` VALUES ('10', '10A 1 gang sakelar bell, putih (E8431BP1)', null);
INSERT INTO `barang` VALUES ('11', '10A sakelar 1 gang - 1 arah, putih (KB31_1_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('12', '10A sakelar 1 gang - 1 arah, putih (KB31_1_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('13', '10A sakelar 1 gang - 1 arah, putih (KB31_1_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('14', '10A sakelar 1 gang - 2 arah, putih (KB31_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('15', '10A sakelar 1 gang - 2 arah, putih (KB31_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('16', '10A sakelar 1 gang - 2 arah, putih (KB31_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('17', '10A sakelar 2 gang - 1 arah, putih (KB32_1_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('18', '10A sakelar 2 gang - 2 arah, putih (KB32_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('19', '10A sakelar 2 gang - 2 arah, putih (KB32_WE_G3)', 'pcs');
INSERT INTO `barang` VALUES ('20', '110 x 253 = 2', null);
INSERT INTO `barang` VALUES ('21', '115/240V shaver unit (KBT727V-OS)', null);
INSERT INTO `barang` VALUES ('22', '115/240V shaver unit (KBT727V-OS)', null);
INSERT INTO `barang` VALUES ('23', '115/240V shaver unit (KBT727V-OS)', null);
INSERT INTO `barang` VALUES ('24', '13A 250V, 1 gang 3 pin universal socket putih (KB413S-OS)', null);
INSERT INTO `barang` VALUES ('25', '13A 250V, 1 gang 3 pin universal socket putih (KB413S-OS)', null);
INSERT INTO `barang` VALUES ('26', '16A 1 gang kotak kontak tipe Schuko, putih (KB426_16S_WE_G3)', null);
INSERT INTO `barang` VALUES ('27', '16A 1 gang kotak kontak tipe Schuko, putih (KB426_16S_WE_G3)', null);
INSERT INTO `barang` VALUES ('28', '16A 1 gang kotak kontak tipe Schuko, putih (KB426_16S_WE_G3)', null);
INSERT INTO `barang` VALUES ('29', '16A 2 gang kotak kontak tipe Schuko, putih (KBT426_16S_WE_G3)', null);
INSERT INTO `barang` VALUES ('30', '16A 2 gang kotak kontak tipe Schuko, putih (KBT426_16S_WE_G3)', null);
INSERT INTO `barang` VALUES ('31', '75 ohm 1 gang - kotak kontak TV, putih (KB31TV-OS)', null);
INSERT INTO `barang` VALUES ('32', '75 ohm 1 gang - kotak kontak TV, putih (KB31TV-OS)', null);
INSERT INTO `barang` VALUES ('33', '75 ohm 1 gang-kotak kontak TV, putih (KB31TV-OS)', null);

-- ----------------------------
-- Table structure for barang_keluar
-- ----------------------------
DROP TABLE IF EXISTS `barang_keluar`;
CREATE TABLE `barang_keluar` (
  `id_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyek` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_keluar`),
  KEY `nip` (`nip`),
  CONSTRAINT `barang_keluar_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `staff` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of barang_keluar
-- ----------------------------
INSERT INTO `barang_keluar` VALUES ('1', '23', '2017-07-19', 'Butuh segera', null);
INSERT INTO `barang_keluar` VALUES ('2', '45', '2017-07-20', 'Urgent', null);
INSERT INTO `barang_keluar` VALUES ('3', '3', '0000-00-00', 'ghjhkj', '123456');
INSERT INTO `barang_keluar` VALUES ('4', '4', '2017-08-23', 'tgjhk', '123456');

-- ----------------------------
-- Table structure for barang_masuk
-- ----------------------------
DROP TABLE IF EXISTS `barang_masuk`;
CREATE TABLE `barang_masuk` (
  `id_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `no_po` varchar(255) DEFAULT NULL,
  `id_subcount` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_masuk`),
  KEY `nip` (`nip`),
  CONSTRAINT `barang_masuk_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `staff` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of barang_masuk
-- ----------------------------
INSERT INTO `barang_masuk` VALUES ('1', '3436363', '34', '2017-07-11', null);
INSERT INTO `barang_masuk` VALUES ('2', '43453', '22', '2017-07-13', null);
INSERT INTO `barang_masuk` VALUES ('3', 'Asas', '4', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('4', 'Asas', '4', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('5', 'oijoi', '9', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('6', 'fhgjkj', '8', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('7', 'fhgjkj', '8', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('8', 'fhgjkj', '8', '0000-00-00', null);
INSERT INTO `barang_masuk` VALUES ('9', 'fhgjkj', '8', '1970-01-01', '123456');
INSERT INTO `barang_masuk` VALUES ('10', 'ijpm', '10', '2017-08-17', '123456');

-- ----------------------------
-- Table structure for detail_keluar
-- ----------------------------
DROP TABLE IF EXISTS `detail_keluar`;
CREATE TABLE `detail_keluar` (
  `id_detail_keluar` int(11) NOT NULL AUTO_INCREMENT,
  `id_keluar` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_detail_keluar`),
  KEY `id_keluar` (`id_keluar`),
  KEY `id_barang` (`id_barang`),
  CONSTRAINT `detail_keluar_ibfk_1` FOREIGN KEY (`id_keluar`) REFERENCES `barang_keluar` (`id_keluar`),
  CONSTRAINT `detail_keluar_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_keluar
-- ----------------------------
INSERT INTO `detail_keluar` VALUES ('1', '3', '5', '7.00', 'ij');
INSERT INTO `detail_keluar` VALUES ('2', '3', '15', '9.00', 'kjb');
INSERT INTO `detail_keluar` VALUES ('3', '4', '4', '78.00', 'yhg');

-- ----------------------------
-- Table structure for detail_masuk
-- ----------------------------
DROP TABLE IF EXISTS `detail_masuk`;
CREATE TABLE `detail_masuk` (
  `id_detail_masuk` int(11) NOT NULL AUTO_INCREMENT,
  `id_masuk` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_detail_masuk`),
  KEY `detail_masuk_ibfk_1` (`id_masuk`),
  KEY `id_barang` (`id_barang`),
  CONSTRAINT `detail_masuk_ibfk_1` FOREIGN KEY (`id_masuk`) REFERENCES `barang_masuk` (`id_masuk`),
  CONSTRAINT `detail_masuk_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_masuk
-- ----------------------------
INSERT INTO `detail_masuk` VALUES ('1', null, '1', '5.00', 'sd');
INSERT INTO `detail_masuk` VALUES ('2', null, '2', '5.00', 'sd');
INSERT INTO `detail_masuk` VALUES ('3', null, '19', '5.00', 'sd');
INSERT INTO `detail_masuk` VALUES ('4', null, '15', '7.00', 'sd');
INSERT INTO `detail_masuk` VALUES ('5', '5', '3', '8.00', 'hbk');
INSERT INTO `detail_masuk` VALUES ('6', '5', '7', '9.00', 'sd');
INSERT INTO `detail_masuk` VALUES ('7', '5', '26', '8.00', 'kh');
INSERT INTO `detail_masuk` VALUES ('10', '10', '3', '8.00', 'kj');
INSERT INTO `detail_masuk` VALUES ('11', '10', '10', '8.00', 'kjn');
INSERT INTO `detail_masuk` VALUES ('12', '10', '20', '9.00', 'kjn');
INSERT INTO `detail_masuk` VALUES ('15', '9', '4', '6.00', 'yf');
INSERT INTO `detail_masuk` VALUES ('16', '9', '19', '9.00', 'h');

-- ----------------------------
-- Table structure for detail_po
-- ----------------------------
DROP TABLE IF EXISTS `detail_po`;
CREATE TABLE `detail_po` (
  `id_detail_po` int(11) NOT NULL AUTO_INCREMENT,
  `id_po` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `jumlah` decimal(15,2) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `keluar` decimal(15,2) DEFAULT NULL,
  `sisa` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id_detail_po`),
  KEY `fk_detail_po_po_1` (`id_po`),
  KEY `fk_detail_po_barang_1` (`id_barang`),
  CONSTRAINT `fk_detail_po_barang_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`),
  CONSTRAINT `fk_detail_po_po_1` FOREIGN KEY (`id_po`) REFERENCES `po` (`id_po`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_po
-- ----------------------------

-- ----------------------------
-- Table structure for detail_pp
-- ----------------------------
DROP TABLE IF EXISTS `detail_pp`;
CREATE TABLE `detail_pp` (
  `id_detail_pp` int(11) NOT NULL AUTO_INCREMENT,
  `id_pp` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `permintaan` decimal(15,2) DEFAULT NULL,
  `diterima` varchar(15) DEFAULT NULL,
  `permintaan_satuan` varchar(20) DEFAULT NULL,
  `diterima_satuan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_detail_pp`),
  KEY `fk_detail_pp_pp_1` (`id_pp`),
  KEY `fk_detail_pp_barang_1` (`id_barang`),
  CONSTRAINT `fk_detail_pp_barang_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`),
  CONSTRAINT `fk_detail_pp_pp_1` FOREIGN KEY (`id_pp`) REFERENCES `pp` (`id_pp`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_pp
-- ----------------------------
INSERT INTO `detail_pp` VALUES ('14', '21', '13', '6.00', null, 'g', null);
INSERT INTO `detail_pp` VALUES ('15', '21', '10', '6.00', null, 'gh', null);
INSERT INTO `detail_pp` VALUES ('16', '22', '12', '6.00', null, 'g', null);
INSERT INTO `detail_pp` VALUES ('17', '22', '13', '7.00', null, 'gf', null);

-- ----------------------------
-- Table structure for gudang
-- ----------------------------
DROP TABLE IF EXISTS `gudang`;
CREATE TABLE `gudang` (
  `id_gudang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_gudang` varchar(20) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_gudang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gudang
-- ----------------------------
INSERT INTO `gudang` VALUES ('1', 'Gudang Induk', '-', '-');

-- ----------------------------
-- Table structure for po
-- ----------------------------
DROP TABLE IF EXISTS `po`;
CREATE TABLE `po` (
  `id_po` int(11) NOT NULL AUTO_INCREMENT,
  `no_po` varchar(50) DEFAULT NULL,
  `id_subcount` int(11) DEFAULT NULL,
  `id_proyek` int(11) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_po`),
  KEY `fk_po_subcount_1` (`id_subcount`),
  KEY `fk_po_proyek_1` (`id_proyek`),
  KEY `nip` (`nip`),
  CONSTRAINT `fk_po_proyek_1` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`),
  CONSTRAINT `fk_po_subcount_1` FOREIGN KEY (`id_subcount`) REFERENCES `subcount` (`id_subcount`),
  CONSTRAINT `po_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `staff` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of po
-- ----------------------------
INSERT INTO `po` VALUES ('1', '08097879', '6', '7', null);
INSERT INTO `po` VALUES ('2', '432525325', '5', '4', null);
INSERT INTO `po` VALUES ('3', '2564364346', '6', '5', null);
INSERT INTO `po` VALUES ('4', '36363643643', '8', '22', null);
INSERT INTO `po` VALUES ('5', '452351234252', '4', '55', null);
INSERT INTO `po` VALUES ('6', '23522362624', '7', '9', null);

-- ----------------------------
-- Table structure for pp
-- ----------------------------
DROP TABLE IF EXISTS `pp`;
CREATE TABLE `pp` (
  `id_pp` int(11) NOT NULL AUTO_INCREMENT,
  `no_pp` varchar(255) DEFAULT NULL,
  `id_proyek` int(11) DEFAULT NULL,
  `id_gudang` int(11) DEFAULT NULL,
  `tanggal_pp` date DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_pp`),
  UNIQUE KEY `no_pp` (`no_pp`),
  KEY `fk_pp_proyek_1` (`id_proyek`),
  KEY `fk_pp_gudang_1` (`id_gudang`),
  KEY `nip` (`nip`),
  CONSTRAINT `fk_pp_gudang_1` FOREIGN KEY (`id_gudang`) REFERENCES `gudang` (`id_gudang`),
  CONSTRAINT `fk_pp_proyek_1` FOREIGN KEY (`id_proyek`) REFERENCES `proyek` (`id_proyek`),
  CONSTRAINT `pp_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `staff` (`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pp
-- ----------------------------
INSERT INTO `pp` VALUES ('10', 'PP.0001', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('12', 'PP.0002', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('13', 'PP.0003', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('14', 'PP.0004', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('15', 'PP.0005', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('16', 'PP.0006', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('17', 'PP.0007', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('18', 'PP.0008', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('19', 'PP.0009', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('20', 'PP.0010', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('21', 'PP.0011', '4', '1', '2017-07-24', null);
INSERT INTO `pp` VALUES ('22', 'PP.0012', '3', '1', '2017-07-24', null);

-- ----------------------------
-- Table structure for proyek
-- ----------------------------
DROP TABLE IF EXISTS `proyek`;
CREATE TABLE `proyek` (
  `id_proyek` int(11) NOT NULL AUTO_INCREMENT,
  `nama_proyek` varchar(50) DEFAULT NULL,
  `alamat_proyek` text,
  `tahun_proyek` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_proyek`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of proyek
-- ----------------------------
INSERT INTO `proyek` VALUES ('1', 'PT Makmuk Abadi Sejahtera', 'Jl Mangga Muda', '2012');
INSERT INTO `proyek` VALUES ('2', 'Egestas Lacinia LLC', '518-6963 Facilisis. Street', '2013');
INSERT INTO `proyek` VALUES ('3', 'Sodales Mauris Consulting', '8592 Sit Road', '2014');
INSERT INTO `proyek` VALUES ('4', 'Orci Luctus Et Incorporated', '918 Nisi Road', '2015');
INSERT INTO `proyek` VALUES ('5', 'Aliquam Auctor LLC', 'P.O. Box 800, 5620 A Rd.', '2016');
INSERT INTO `proyek` VALUES ('6', 'Consequat Dolor Vitae LLC', 'P.O. Box 742, 1360 Ut St.', '2017');
INSERT INTO `proyek` VALUES ('7', 'Lacus Etiam Inc.', '327 Lacus. Road', '2018');
INSERT INTO `proyek` VALUES ('8', 'Mauris PC', '801-2951 Tempor Rd.', '2012');
INSERT INTO `proyek` VALUES ('9', 'Augue Id Ante LLC', 'P.O. Box 835, 7692 Malesuada Ave', '2013');
INSERT INTO `proyek` VALUES ('10', 'Ultrices Mauris Ipsum Incorporated', '876-7384 A, Rd.', '2014');
INSERT INTO `proyek` VALUES ('11', 'Et Magnis Limited', '263-6768 Non Avenue', '2015');
INSERT INTO `proyek` VALUES ('12', 'Feugiat Corporation', 'P.O. Box 462, 7268 Dictum St.', '2016');
INSERT INTO `proyek` VALUES ('13', 'Lacus LLC', 'Ap #851-4083 Ante. Street', '2017');
INSERT INTO `proyek` VALUES ('14', 'Ante Vivamus Non PC', '657-2631 Lorem Road', '2018');
INSERT INTO `proyek` VALUES ('15', 'Quisque Porttitor Eros Consulting', '9729 Neque Rd.', '2012');
INSERT INTO `proyek` VALUES ('16', 'Amet Metus Aliquam LLC', 'P.O. Box 172, 6526 Ut, Road', '2013');
INSERT INTO `proyek` VALUES ('17', 'Orci Consulting', 'P.O. Box 538, 3145 Nulla Road', '2014');
INSERT INTO `proyek` VALUES ('18', 'In Industries', 'P.O. Box 229, 904 Elit, St.', '2015');
INSERT INTO `proyek` VALUES ('19', 'Nisi Magna LLC', 'Ap #266-1516 Mauris Rd.', '2016');
INSERT INTO `proyek` VALUES ('20', 'Dui Nec Tempus Associates', '251-2223 Sit St.', '2017');
INSERT INTO `proyek` VALUES ('21', 'Vestibulum Institute', 'Ap #207-7854 Ut Street', '2018');
INSERT INTO `proyek` VALUES ('22', 'Sem Corp.', 'P.O. Box 539, 5537 Congue Avenue', '2012');
INSERT INTO `proyek` VALUES ('23', 'Per Conubia Nostra Industries', 'Ap #193-5348 Egestas. Rd.', '2013');
INSERT INTO `proyek` VALUES ('24', 'Pharetra Nibh LLC', '593-3917 Urna. St.', '2014');
INSERT INTO `proyek` VALUES ('25', 'Commodo Ltd', '699-614 Ante. St.', '2015');
INSERT INTO `proyek` VALUES ('26', 'Nibh Corp.', '598-3914 Iaculis Street', '2016');
INSERT INTO `proyek` VALUES ('27', 'Odio Auctor Vitae Consulting', '237-9918 Gravida Av.', '2017');
INSERT INTO `proyek` VALUES ('28', 'Facilisis Non LLP', 'Ap #476-850 Enim. Road', '2018');
INSERT INTO `proyek` VALUES ('29', 'Consectetuer Mauris Id Corp.', 'Ap #893-5082 A, St.', '2012');
INSERT INTO `proyek` VALUES ('30', 'Eu Neque Corporation', 'Ap #847-8962 Vitae, Av.', '2013');
INSERT INTO `proyek` VALUES ('31', 'Amet Dapibus LLP', '340-2066 Molestie Ave', '2014');
INSERT INTO `proyek` VALUES ('32', 'Et Ultrices Institute', '7035 Adipiscing Road', '2015');
INSERT INTO `proyek` VALUES ('33', 'Orci Institute', 'Ap #738-8138 Eu, Rd.', '2016');
INSERT INTO `proyek` VALUES ('34', 'Urna PC', 'P.O. Box 129, 9181 Donec Av.', '2017');
INSERT INTO `proyek` VALUES ('35', 'Sem Vitae Aliquam Associates', 'Ap #380-4888 Eleifend, Rd.', '2018');
INSERT INTO `proyek` VALUES ('36', 'Sed Foundation', '1322 Orci Av.', '2012');
INSERT INTO `proyek` VALUES ('37', 'A Feugiat Tellus PC', 'P.O. Box 362, 6887 Tempus St.', '2013');
INSERT INTO `proyek` VALUES ('38', 'Mauris Sapien Cursus Associates', '132-6661 Fusce Avenue', '2014');
INSERT INTO `proyek` VALUES ('39', 'Vulputate Institute', 'Ap #496-2472 Faucibus Rd.', '2015');
INSERT INTO `proyek` VALUES ('40', 'Aliquam Rutrum Consulting', 'Ap #182-6715 Lobortis Road', '2016');
INSERT INTO `proyek` VALUES ('41', 'Justo Faucibus Associates', 'Ap #759-3027 Natoque St.', '2017');
INSERT INTO `proyek` VALUES ('42', 'Felis Orci Associates', '5245 Faucibus Avenue', '2018');
INSERT INTO `proyek` VALUES ('43', 'Consectetuer Mauris Corp.', '372-2907 Aenean Street', '2012');
INSERT INTO `proyek` VALUES ('44', 'Eget Magna Limited', 'Ap #678-7869 Mus. St.', '2013');
INSERT INTO `proyek` VALUES ('45', 'Vestibulum Lorem Sit Foundation', 'P.O. Box 782, 5879 Vel, Street', '2014');
INSERT INTO `proyek` VALUES ('46', 'Venenatis Vel LLC', '1154 Et Avenue', '2015');
INSERT INTO `proyek` VALUES ('47', 'Phasellus Vitae Mauris Incorporated', 'P.O. Box 904, 4551 Nibh. Street', '2016');
INSERT INTO `proyek` VALUES ('48', 'Ante Ipsum Associates', '8759 Eget Av.', '2017');
INSERT INTO `proyek` VALUES ('49', 'Hymenaeos Mauris Ut Incorporated', 'Ap #284-8840 Eu, Av.', '2018');
INSERT INTO `proyek` VALUES ('50', 'Id Magna Associates', '3098 Ante, Avenue', '2012');
INSERT INTO `proyek` VALUES ('51', 'Sit Amet Massa Corporation', 'P.O. Box 336, 5155 Consequat Rd.', '2013');
INSERT INTO `proyek` VALUES ('52', 'Venenatis A LLP', 'Ap #867-4532 Vel Rd.', '2014');
INSERT INTO `proyek` VALUES ('53', 'Donec Consectetuer Consulting', '510-9215 Aliquam St.', '2015');
INSERT INTO `proyek` VALUES ('54', 'Et Inc.', 'P.O. Box 131, 5006 Mollis. Avenue', '2016');
INSERT INTO `proyek` VALUES ('55', 'Nunc LLP', 'Ap #727-849 Egestas Road', '2017');
INSERT INTO `proyek` VALUES ('56', 'Iaculis Consulting', '4229 Pellentesque. Avenue', '2018');
INSERT INTO `proyek` VALUES ('57', 'In At Pede Ltd', '130-4160 In St.', '2012');
INSERT INTO `proyek` VALUES ('58', 'Suspendisse Corp.', 'P.O. Box 635, 8324 Varius St.', '2013');
INSERT INTO `proyek` VALUES ('59', 'Sed Ltd', '846-8667 Leo. Av.', '2014');
INSERT INTO `proyek` VALUES ('60', 'Risus Donec Egestas Limited', 'Ap #948-8148 Quam Rd.', '2015');
INSERT INTO `proyek` VALUES ('61', 'Amet Consectetuer LLP', '4175 Lectus Street', '2016');
INSERT INTO `proyek` VALUES ('62', 'A Sollicitudin Orci PC', 'P.O. Box 998, 5170 Sem. Rd.', '2017');
INSERT INTO `proyek` VALUES ('63', 'Mi Corp.', 'Ap #242-8868 Augue Rd.', '2018');
INSERT INTO `proyek` VALUES ('64', 'Convallis Corporation', 'Ap #868-2920 Aliquet Rd.', '2012');
INSERT INTO `proyek` VALUES ('65', 'Eu Turpis Corp.', 'P.O. Box 566, 1338 Molestie St.', '2013');
INSERT INTO `proyek` VALUES ('66', 'Montes Industries', 'Ap #503-1718 Duis Ave', '2014');
INSERT INTO `proyek` VALUES ('67', 'Ac Tellus Institute', 'Ap #117-3129 Dolor Rd.', '2015');
INSERT INTO `proyek` VALUES ('68', 'Cras Associates', '9968 Congue Street', '2016');
INSERT INTO `proyek` VALUES ('69', 'Nullam Institute', '2729 Et Av.', '2017');
INSERT INTO `proyek` VALUES ('70', 'Et Nunc Quisque Foundation', '766-2490 Netus St.', '2018');
INSERT INTO `proyek` VALUES ('71', 'Ultrices LLP', 'Ap #447-2420 Diam. Av.', '2012');
INSERT INTO `proyek` VALUES ('72', 'Gravida Nunc Ltd', '1988 Natoque Avenue', '2013');
INSERT INTO `proyek` VALUES ('73', 'In Dolor Inc.', 'Ap #173-7626 Dolor. Street', '2014');
INSERT INTO `proyek` VALUES ('74', 'Ut Semper Institute', '1244 Nunc Rd.', '2015');
INSERT INTO `proyek` VALUES ('75', 'Quisque Company', 'P.O. Box 433, 5482 Vitae Rd.', '2016');
INSERT INTO `proyek` VALUES ('76', 'Metus Facilisis Lorem Incorporated', 'P.O. Box 513, 6777 Nunc Rd.', '2017');
INSERT INTO `proyek` VALUES ('77', 'Congue Company', '868-6429 Molestie Av.', '2018');
INSERT INTO `proyek` VALUES ('78', 'Interdum Libero Dui Company', 'P.O. Box 285, 2278 Elit, St.', '2012');
INSERT INTO `proyek` VALUES ('79', 'Dictum LLC', '6506 Tincidunt Rd.', '2013');
INSERT INTO `proyek` VALUES ('80', 'At Velit Limited', 'P.O. Box 368, 4805 Leo Street', '2014');
INSERT INTO `proyek` VALUES ('81', 'Ipsum Curabitur Consequat Institute', 'P.O. Box 208, 9106 Felis Avenue', '2015');
INSERT INTO `proyek` VALUES ('82', 'Nibh Sit Company', 'P.O. Box 723, 5623 Ultricies Avenue', '2016');
INSERT INTO `proyek` VALUES ('83', 'Dolor LLC', 'Ap #338-3542 Imperdiet Ave', '2017');
INSERT INTO `proyek` VALUES ('84', 'Tincidunt Nunc LLP', '723 Aliquam St.', '2018');
INSERT INTO `proyek` VALUES ('85', 'Elementum Sem LLC', '7252 Consectetuer Ave', '2012');
INSERT INTO `proyek` VALUES ('86', 'Lectus Consulting', 'Ap #255-8515 Ridiculus Av.', '2013');
INSERT INTO `proyek` VALUES ('87', 'Dictum Augue Malesuada Limited', 'P.O. Box 581, 9303 Blandit St.', '2014');
INSERT INTO `proyek` VALUES ('88', 'Iaculis Enim Sit LLC', '9206 Aliquam St.', '2015');
INSERT INTO `proyek` VALUES ('89', 'In Corp.', 'Ap #695-5771 Molestie Ave', '2016');
INSERT INTO `proyek` VALUES ('90', 'Mauris Molestie Pharetra Limited', 'P.O. Box 456, 6567 Nunc Rd.', '2017');
INSERT INTO `proyek` VALUES ('91', 'Magna A Corporation', 'Ap #191-382 Cum Rd.', '2018');
INSERT INTO `proyek` VALUES ('92', 'Consectetuer Ltd', 'Ap #360-7795 Convallis Street', '2012');
INSERT INTO `proyek` VALUES ('93', 'Dis Institute', '7617 Adipiscing Road', '2013');
INSERT INTO `proyek` VALUES ('94', 'Sed Dictum LLC', '769-4910 Orci St.', '2014');
INSERT INTO `proyek` VALUES ('95', 'Dolor Foundation', '605-2893 Sapien, Ave', '2015');
INSERT INTO `proyek` VALUES ('96', 'Enim Condimentum Eget Associates', 'Ap #999-5738 Varius Avenue', '2016');
INSERT INTO `proyek` VALUES ('97', 'Fames Inc.', '5339 Vel St.', '2017');
INSERT INTO `proyek` VALUES ('98', 'Ullamcorper Institute', '635-5125 Id Rd.', '2018');
INSERT INTO `proyek` VALUES ('99', 'Aliquam Erat Foundation', 'Ap #224-6925 Nisi Ave', '2012');
INSERT INTO `proyek` VALUES ('100', 'A Auctor Industries', '541-9449 In Street', '2013');
INSERT INTO `proyek` VALUES ('101', 'In Inc.', '2397 Tellus, St.', '2014');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `nip` varchar(20) NOT NULL,
  `nama_staff` varchar(100) DEFAULT NULL,
  `bagian` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `alamat` text,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `hak_akses` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('', 'SDsd', 'sdsd', 'staff_1501422984.JPG', 'asdsd', 'sds', 'dasfsa', 'gudang');
INSERT INTO `staff` VALUES ('1122445', 'Purchasing', 'Purchasing', 'as', 'sas', 'purchasing', 'purchasing', 'purchasing');
INSERT INTO `staff` VALUES ('123456', 'Gudang', 'Gudang', 'vbb', 'cvbcvb', 'gudang', 'gudang', 'gudang');
INSERT INTO `staff` VALUES ('2355', 'Project Manager', null, 'sd', null, 'pm', 'pm', 'project_manager');
INSERT INTO `staff` VALUES ('f', 'Administrator', 'Admin', 'sd', 'asdsd', 'admin', 'admin', 'admin');

-- ----------------------------
-- Table structure for stok_gudang
-- ----------------------------
DROP TABLE IF EXISTS `stok_gudang`;
CREATE TABLE `stok_gudang` (
  `id_stok` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_gudang` int(11) DEFAULT NULL,
  `stok` decimal(15,2) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stok_gudang
-- ----------------------------

-- ----------------------------
-- Table structure for subcount
-- ----------------------------
DROP TABLE IF EXISTS `subcount`;
CREATE TABLE `subcount` (
  `id_subcount` int(11) NOT NULL AUTO_INCREMENT,
  `nama_subcount` varchar(100) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_subcount`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of subcount
-- ----------------------------
INSERT INTO `subcount` VALUES ('2', 'PT Makmuk Abadi Sejahtera', 'Jl Mangga Muda', '081989888');
INSERT INTO `subcount` VALUES ('3', 'Egestas Lacinia LLC', '518-6963 Facilisis. Street', '048-361-1069');
INSERT INTO `subcount` VALUES ('4', 'Sodales Mauris Consulting', '8592 Sit Road', '004-892-1044');
INSERT INTO `subcount` VALUES ('5', 'Orci Luctus Et Incorporated', '918 Nisi Road', '010-326-0860');
INSERT INTO `subcount` VALUES ('6', 'Aliquam Auctor LLC', 'P.O. Box 800, 5620 A Rd.', '033-402-9722');
INSERT INTO `subcount` VALUES ('7', 'Consequat Dolor Vitae LLC', 'P.O. Box 742, 1360 Ut St.', '060-404-2498');
INSERT INTO `subcount` VALUES ('8', 'Lacus Etiam Inc.', '327 Lacus. Road', '080-318-0002');
INSERT INTO `subcount` VALUES ('9', 'Mauris PC', '801-2951 Tempor Rd.', '015-157-0320');
INSERT INTO `subcount` VALUES ('10', 'Augue Id Ante LLC', 'P.O. Box 835, 7692 Malesuada Ave', '018-388-2565');
INSERT INTO `subcount` VALUES ('11', 'Ultrices Mauris Ipsum Incorporated', '876-7384 A, Rd.', '054-493-6208');
INSERT INTO `subcount` VALUES ('12', 'Et Magnis Limited', '263-6768 Non Avenue', '034-209-7644');
INSERT INTO `subcount` VALUES ('13', 'Feugiat Corporation', 'P.O. Box 462, 7268 Dictum St.', '097-378-4088');
INSERT INTO `subcount` VALUES ('14', 'Lacus LLC', 'Ap #851-4083 Ante. Street', '056-074-9991');
INSERT INTO `subcount` VALUES ('15', 'Ante Vivamus Non PC', '657-2631 Lorem Road', '011-401-9313');
INSERT INTO `subcount` VALUES ('16', 'Quisque Porttitor Eros Consulting', '9729 Neque Rd.', '025-983-8839');
INSERT INTO `subcount` VALUES ('17', 'Amet Metus Aliquam LLC', 'P.O. Box 172, 6526 Ut, Road', '006-965-9262');
INSERT INTO `subcount` VALUES ('18', 'Orci Consulting', 'P.O. Box 538, 3145 Nulla Road', '058-804-6546');
INSERT INTO `subcount` VALUES ('19', 'In Industries', 'P.O. Box 229, 904 Elit, St.', '089-181-5598');
INSERT INTO `subcount` VALUES ('20', 'Nisi Magna LLC', 'Ap #266-1516 Mauris Rd.', '022-252-0284');
INSERT INTO `subcount` VALUES ('21', 'Dui Nec Tempus Associates', '251-2223 Sit St.', '069-324-4479');
INSERT INTO `subcount` VALUES ('22', 'Vestibulum Institute', 'Ap #207-7854 Ut Street', '013-174-5468');
INSERT INTO `subcount` VALUES ('23', 'Sem Corp.', 'P.O. Box 539, 5537 Congue Avenue', '015-946-2414');
INSERT INTO `subcount` VALUES ('24', 'Per Conubia Nostra Industries', 'Ap #193-5348 Egestas. Rd.', '009-355-2655');
INSERT INTO `subcount` VALUES ('25', 'Pharetra Nibh LLC', '593-3917 Urna. St.', '032-300-8806');
INSERT INTO `subcount` VALUES ('26', 'Commodo Ltd', '699-614 Ante. St.', '093-053-5433');
INSERT INTO `subcount` VALUES ('27', 'Nibh Corp.', '598-3914 Iaculis Street', '058-085-7344');
INSERT INTO `subcount` VALUES ('28', 'Odio Auctor Vitae Consulting', '237-9918 Gravida Av.', '013-815-8372');
INSERT INTO `subcount` VALUES ('29', 'Facilisis Non LLP', 'Ap #476-850 Enim. Road', '035-579-6195');
INSERT INTO `subcount` VALUES ('30', 'Consectetuer Mauris Id Corp.', 'Ap #893-5082 A, St.', '055-985-5604');
INSERT INTO `subcount` VALUES ('31', 'Eu Neque Corporation', 'Ap #847-8962 Vitae, Av.', '082-961-4574');
INSERT INTO `subcount` VALUES ('32', 'Amet Dapibus LLP', '340-2066 Molestie Ave', '011-229-0013');
INSERT INTO `subcount` VALUES ('33', 'Et Ultrices Institute', '7035 Adipiscing Road', '004-584-9437');
INSERT INTO `subcount` VALUES ('34', 'Orci Institute', 'Ap #738-8138 Eu, Rd.', '096-277-8460');
INSERT INTO `subcount` VALUES ('35', 'Urna PC', 'P.O. Box 129, 9181 Donec Av.', '046-151-1125');
INSERT INTO `subcount` VALUES ('36', 'Sem Vitae Aliquam Associates', 'Ap #380-4888 Eleifend, Rd.', '028-520-5535');
INSERT INTO `subcount` VALUES ('37', 'Sed Foundation', '1322 Orci Av.', '092-025-4570');
INSERT INTO `subcount` VALUES ('38', 'A Feugiat Tellus PC', 'P.O. Box 362, 6887 Tempus St.', '093-723-1771');
INSERT INTO `subcount` VALUES ('39', 'Mauris Sapien Cursus Associates', '132-6661 Fusce Avenue', '096-284-6178');
INSERT INTO `subcount` VALUES ('40', 'Vulputate Institute', 'Ap #496-2472 Faucibus Rd.', '050-954-4306');
INSERT INTO `subcount` VALUES ('41', 'Aliquam Rutrum Consulting', 'Ap #182-6715 Lobortis Road', '026-092-1057');
INSERT INTO `subcount` VALUES ('42', 'Justo Faucibus Associates', 'Ap #759-3027 Natoque St.', '022-127-6620');
INSERT INTO `subcount` VALUES ('43', 'Felis Orci Associates', '5245 Faucibus Avenue', '089-739-6373');
INSERT INTO `subcount` VALUES ('44', 'Consectetuer Mauris Corp.', '372-2907 Aenean Street', '061-521-4950');
INSERT INTO `subcount` VALUES ('45', 'Eget Magna Limited', 'Ap #678-7869 Mus. St.', '053-377-7533');
INSERT INTO `subcount` VALUES ('46', 'Vestibulum Lorem Sit Foundation', 'P.O. Box 782, 5879 Vel, Street', '075-522-3550');
INSERT INTO `subcount` VALUES ('47', 'Venenatis Vel LLC', '1154 Et Avenue', '022-425-7200');
INSERT INTO `subcount` VALUES ('48', 'Phasellus Vitae Mauris Incorporated', 'P.O. Box 904, 4551 Nibh. Street', '086-795-5643');
INSERT INTO `subcount` VALUES ('49', 'Ante Ipsum Associates', '8759 Eget Av.', '003-417-2948');
INSERT INTO `subcount` VALUES ('50', 'Hymenaeos Mauris Ut Incorporated', 'Ap #284-8840 Eu, Av.', '073-707-4735');
INSERT INTO `subcount` VALUES ('51', 'Id Magna Associates', '3098 Ante, Avenue', '066-967-2107');
INSERT INTO `subcount` VALUES ('52', 'Sit Amet Massa Corporation', 'P.O. Box 336, 5155 Consequat Rd.', '014-828-4845');
INSERT INTO `subcount` VALUES ('53', 'Venenatis A LLP', 'Ap #867-4532 Vel Rd.', '040-735-5281');
INSERT INTO `subcount` VALUES ('54', 'Donec Consectetuer Consulting', '510-9215 Aliquam St.', '064-079-6563');
INSERT INTO `subcount` VALUES ('55', 'Et Inc.', 'P.O. Box 131, 5006 Mollis. Avenue', '095-579-8577');
INSERT INTO `subcount` VALUES ('56', 'Nunc LLP', 'Ap #727-849 Egestas Road', '017-892-5842');
INSERT INTO `subcount` VALUES ('57', 'Iaculis Consulting', '4229 Pellentesque. Avenue', '079-217-2148');
INSERT INTO `subcount` VALUES ('58', 'In At Pede Ltd', '130-4160 In St.', '014-211-5688');
INSERT INTO `subcount` VALUES ('59', 'Suspendisse Corp.', 'P.O. Box 635, 8324 Varius St.', '077-958-0237');
INSERT INTO `subcount` VALUES ('60', 'Sed Ltd', '846-8667 Leo. Av.', '045-392-9180');
INSERT INTO `subcount` VALUES ('61', 'Risus Donec Egestas Limited', 'Ap #948-8148 Quam Rd.', '035-432-5877');
INSERT INTO `subcount` VALUES ('62', 'Amet Consectetuer LLP', '4175 Lectus Street', '072-164-6682');
INSERT INTO `subcount` VALUES ('63', 'A Sollicitudin Orci PC', 'P.O. Box 998, 5170 Sem. Rd.', '042-065-2172');
INSERT INTO `subcount` VALUES ('64', 'Mi Corp.', 'Ap #242-8868 Augue Rd.', '031-839-7711');
INSERT INTO `subcount` VALUES ('65', 'Convallis Corporation', 'Ap #868-2920 Aliquet Rd.', '042-031-1413');
INSERT INTO `subcount` VALUES ('66', 'Eu Turpis Corp.', 'P.O. Box 566, 1338 Molestie St.', '060-613-0436');
INSERT INTO `subcount` VALUES ('67', 'Montes Industries', 'Ap #503-1718 Duis Ave', '035-617-1922');
INSERT INTO `subcount` VALUES ('68', 'Ac Tellus Institute', 'Ap #117-3129 Dolor Rd.', '037-838-6224');
INSERT INTO `subcount` VALUES ('69', 'Cras Associates', '9968 Congue Street', '038-361-0022');
INSERT INTO `subcount` VALUES ('70', 'Nullam Institute', '2729 Et Av.', '066-258-4045');
INSERT INTO `subcount` VALUES ('71', 'Et Nunc Quisque Foundation', '766-2490 Netus St.', '024-303-4316');
INSERT INTO `subcount` VALUES ('72', 'Ultrices LLP', 'Ap #447-2420 Diam. Av.', '074-824-6386');
INSERT INTO `subcount` VALUES ('73', 'Gravida Nunc Ltd', '1988 Natoque Avenue', '059-510-0470');
INSERT INTO `subcount` VALUES ('74', 'In Dolor Inc.', 'Ap #173-7626 Dolor. Street', '021-772-4398');
INSERT INTO `subcount` VALUES ('75', 'Ut Semper Institute', '1244 Nunc Rd.', '005-024-1066');
INSERT INTO `subcount` VALUES ('76', 'Quisque Company', 'P.O. Box 433, 5482 Vitae Rd.', '029-242-4965');
INSERT INTO `subcount` VALUES ('77', 'Metus Facilisis Lorem Incorporated', 'P.O. Box 513, 6777 Nunc Rd.', '070-594-5829');
INSERT INTO `subcount` VALUES ('78', 'Congue Company', '868-6429 Molestie Av.', '044-902-4431');
INSERT INTO `subcount` VALUES ('79', 'Interdum Libero Dui Company', 'P.O. Box 285, 2278 Elit, St.', '064-609-7143');
INSERT INTO `subcount` VALUES ('80', 'Dictum LLC', '6506 Tincidunt Rd.', '013-395-9365');
INSERT INTO `subcount` VALUES ('81', 'At Velit Limited', 'P.O. Box 368, 4805 Leo Street', '030-588-0688');
INSERT INTO `subcount` VALUES ('82', 'Ipsum Curabitur Consequat Institute', 'P.O. Box 208, 9106 Felis Avenue', '049-802-7505');
INSERT INTO `subcount` VALUES ('83', 'Nibh Sit Company', 'P.O. Box 723, 5623 Ultricies Avenue', '054-618-0189');
INSERT INTO `subcount` VALUES ('84', 'Dolor LLC', 'Ap #338-3542 Imperdiet Ave', '091-733-9016');
INSERT INTO `subcount` VALUES ('85', 'Tincidunt Nunc LLP', '723 Aliquam St.', '067-668-2774');
INSERT INTO `subcount` VALUES ('86', 'Elementum Sem LLC', '7252 Consectetuer Ave', '069-051-5487');
INSERT INTO `subcount` VALUES ('87', 'Lectus Consulting', 'Ap #255-8515 Ridiculus Av.', '033-901-8077');
INSERT INTO `subcount` VALUES ('88', 'Dictum Augue Malesuada Limited', 'P.O. Box 581, 9303 Blandit St.', '085-906-0592');
INSERT INTO `subcount` VALUES ('89', 'Iaculis Enim Sit LLC', '9206 Aliquam St.', '005-600-2615');
INSERT INTO `subcount` VALUES ('90', 'In Corp.', 'Ap #695-5771 Molestie Ave', '099-391-2585');
INSERT INTO `subcount` VALUES ('91', 'Mauris Molestie Pharetra Limited', 'P.O. Box 456, 6567 Nunc Rd.', '060-713-7049');
INSERT INTO `subcount` VALUES ('92', 'Magna A Corporation', 'Ap #191-382 Cum Rd.', '080-901-3903');
INSERT INTO `subcount` VALUES ('93', 'Consectetuer Ltd', 'Ap #360-7795 Convallis Street', '002-197-7296');
INSERT INTO `subcount` VALUES ('94', 'Dis Institute', '7617 Adipiscing Road', '011-044-8593');
INSERT INTO `subcount` VALUES ('95', 'Sed Dictum LLC', '769-4910 Orci St.', '031-128-5954');
INSERT INTO `subcount` VALUES ('96', 'Dolor Foundation', '605-2893 Sapien, Ave', '083-826-2628');
INSERT INTO `subcount` VALUES ('97', 'Enim Condimentum Eget Associates', 'Ap #999-5738 Varius Avenue', '004-766-4821');
INSERT INTO `subcount` VALUES ('98', 'Fames Inc.', '5339 Vel St.', '076-180-8128');
INSERT INTO `subcount` VALUES ('99', 'Ullamcorper Institute', '635-5125 Id Rd.', '075-167-3049');
INSERT INTO `subcount` VALUES ('100', 'Aliquam Erat Foundation', 'Ap #224-6925 Nisi Ave', '043-349-8152');
INSERT INTO `subcount` VALUES ('101', 'A Auctor Industries', '541-9449 In Street', '006-832-8521');
INSERT INTO `subcount` VALUES ('102', 'In Inc.', '2397 Tellus, St.', '053-585-3253');
SET FOREIGN_KEY_CHECKS=1;
